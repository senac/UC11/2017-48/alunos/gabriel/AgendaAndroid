package com.example.sala304b.agenda.dao;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import com.example.sala304b.agenda.model.Agenda;

import java.util.ArrayList;
import java.util.List;

public class AgendaDAO extends SQLiteOpenHelper{

    private static final String DATABASE = "SQLiteAgenda";
    private static int VERSION = 1;

    public AgendaDAO(Context context) {
        super(context, DATABASE, null, VERSION);
    }


    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        String ddl = "CREATE TABLE Agenda (id INTEGER PRIMARY KEY, nome TEXT NOT NULL, imagem TEXT, endereco TEXT, telefone TEXT, celular TEXT); ";
        Log.i("#SQL" , ddl );
        sqLiteDatabase.execSQL(ddl);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {
        String ddl = "DROP TABLE IF EXISTS Agenda;";
        Log.i("#SQL" , ddl );
        sqLiteDatabase.execSQL(ddl);
        this.onCreate(sqLiteDatabase);
    }


    public void salvar (Agenda agenda){
        ContentValues values = new ContentValues();

        values.put("nome", agenda.getNome());
        values.put("imagem", agenda.getImagem());
        values.put("endereco", agenda.getEndereço());
        values.put("telefone", agenda.getTelefone());
        values.put("celular", agenda.getCelular());

        if(agenda.getId() == 0) {
            getWritableDatabase().insert("Agenda", null, values);
        }else{
            getWritableDatabase().update("Agenda", values," id = " + agenda.getId(), null);
        }
    }



    public List<Agenda> preencherLista(){

        List<Agenda> lista = new ArrayList<>();

        String colunas[] = {"id", "nome", "endereco", "telefone", "celular", "imagem"};

        Cursor cursor = getWritableDatabase().query("Agenda", colunas, null, null, null, null,null);

        while(cursor.moveToNext()){

            Agenda agenda = new Agenda();

            agenda.setId(cursor.getInt(0));
            agenda.setNome(cursor.getString(1));
            agenda.setEndereço(cursor.getString(2));
            agenda.setTelefone(cursor.getString(3));
            agenda.setCelular(cursor.getString(4));
            agenda.setImagem(cursor.getString(5));

            lista.add(agenda);

        }

        return lista;
    }
}
