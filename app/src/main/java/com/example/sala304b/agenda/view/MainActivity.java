package com.example.sala304b.agenda.view;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import com.example.sala304b.agenda.R;
import com.example.sala304b.agenda.dao.AgendaDAO;
import com.example.sala304b.agenda.model.Agenda;

import java.util.ArrayList;
import java.util.List;

public class MainActivity extends AppCompatActivity {

    public static final String AGENDA = "agenda" ;


    private ListView listView;
    private ArrayAdapter<Agenda> adapter;
    private List<Agenda> lista = new ArrayList<>();

    private AgendaDAO dao;

    private Agenda contatoSelecionado;




    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        listView = findViewById(R.id.listView);

        listView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> adapterView, View view, int posicao, long l) {

                contatoSelecionado = (Agenda) adapterView.getItemAtPosition(posicao);

                return false;
            }
        });

        registerForContextMenu(listView);

        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> adapter, View contexto, int posicao, long indice) {

                Agenda agenda = (Agenda) adapter.getItemAtPosition(posicao) ;

                Intent intent = new Intent(MainActivity.this , DetalheActivity.class) ;
                intent.putExtra(AGENDA , agenda);

                startActivity(intent);
            }
        });


    }

    @Override
    protected void onResume() {
        super.onResume();

        listView = findViewById(R.id.listView);

        dao = new AgendaDAO(this);
        lista = dao.preencherLista();
        dao.close();

        adapter = new ArrayAdapter<Agenda>(this, android.R.layout.simple_list_item_1, lista);

        listView.setAdapter(adapter);
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {

        MenuItem menuEditar = menu.add("Editar");

        menuEditar.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intent = new Intent(MainActivity.this, NovoActivity.class);

                intent.putExtra(AGENDA, contatoSelecionado);
                startActivity(intent);

                return false;
            }
        });

        MenuItem menuLigarTelefone = menu.add("Ligar para o Telefone");

        menuLigarTelefone.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intentDeLigar = new Intent(Intent.ACTION_CALL);

                Uri discar = Uri.parse("tel:" + contatoSelecionado.getTelefone());
                intentDeLigar.setData(discar);
                startActivity(intentDeLigar);

                return false;
            }
        }) ;



        MenuItem mandarSmsTelefone =  menu.add("Enviar SMS para o Telefone");

        mandarSmsTelefone.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intentMandarSMS = new Intent(Intent.ACTION_SENDTO);
                Uri enviarSMS = Uri.parse("sms:" + contatoSelecionado.getTelefone());
                intentMandarSMS.putExtra("sms_body", "Ola " + contatoSelecionado + " !");
                intentMandarSMS.setData(enviarSMS);
                startActivity(intentMandarSMS);

                return false;
            }
        });

        MenuItem menuLigarCelular = menu.add("Ligar para o Celular");

        menuLigarCelular.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intentDeLigar = new Intent(Intent.ACTION_CALL);

                Uri discar = Uri.parse("tel:" + contatoSelecionado.getCelular());
                intentDeLigar.setData(discar);
                startActivity(intentDeLigar);

                return false;
            }
        }) ;



        MenuItem mandarSmsCelular =  menu.add("Enviar SMS para o Celular");

        mandarSmsCelular.setOnMenuItemClickListener(new MenuItem.OnMenuItemClickListener() {
            @Override
            public boolean onMenuItemClick(MenuItem menuItem) {
                Intent intentMandarSMS = new Intent(Intent.ACTION_SENDTO);
                Uri enviarSMS = Uri.parse("sms:" + contatoSelecionado.getCelular());
                intentMandarSMS.putExtra("sms_body", "Ola " + contatoSelecionado + " !");
                intentMandarSMS.setData(enviarSMS);
                startActivity(intentMandarSMS);

                return false;
            }
        });


    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.menu, menu);
        return true;
    }

    public void novo (MenuItem menuItem){

        Intent intent = new Intent(this , NovoActivity.class);
        startActivityForResult(intent, RESULT_OK);
    }

    public void sobre(MenuItem menuItem){

    }
}
