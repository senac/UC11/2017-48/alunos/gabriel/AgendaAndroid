package com.example.sala304b.agenda.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.sala304b.agenda.R;
import com.example.sala304b.agenda.model.Agenda;

public class DetalheActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_detalhe);

        Intent intent = getIntent();

        Agenda agenda = (Agenda) intent.getSerializableExtra(MainActivity.AGENDA);


        TextView nomeTxt = findViewById(R.id.nomeTxt);
        TextView enderecoTxt = findViewById(R.id.enderecoTxt);
        TextView telefoneTxt = findViewById(R.id.telefoneTxt);
        TextView celularTxt = findViewById(R.id.celularTxt);
        ImageView imageView = findViewById(R.id.imageView);

        nomeTxt.setText(agenda.getNome());
        enderecoTxt.setText(agenda.getEndereço());
        telefoneTxt.setText(agenda.getTelefone());
        celularTxt.setText(agenda.getCelular());

        if(agenda.getImagem() != null){
            Bitmap fotoOri = BitmapFactory.decodeFile(agenda.getImagem()) ;

            Bitmap fotoFormt = Bitmap.createScaledBitmap(fotoOri , 200 , 200 , true);

            imageView.setImageBitmap(fotoFormt);
        }
    }
}
