package com.example.sala304b.agenda.view;

import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.net.Uri;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.example.sala304b.agenda.R;
import com.example.sala304b.agenda.dao.AgendaDAO;
import com.example.sala304b.agenda.model.Agenda;

import java.io.File;

public class NovoActivity extends AppCompatActivity {

    private static final int CAPTURA_IMAGEM = 1  ;

    private EditText nomeTxt;
    private EditText enderecoTxt;
    private EditText telefoneTxt;
    private EditText celularTxt;
    private ImageView imageView;

    private Agenda agenda;

    private AgendaDAO dao;

    private String nomeArquivo;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_novo);

        imageView = findViewById(R.id.imageView);
        nomeTxt = findViewById(R.id.nomeTxt);
        enderecoTxt = findViewById(R.id.enderecoTxt);
        telefoneTxt = findViewById(R.id.telefoneTxt);
        celularTxt = findViewById(R.id.celularTxt);

        Intent intent = getIntent();

        agenda = (Agenda) intent.getSerializableExtra(MainActivity.AGENDA);

        if(agenda == null){
            agenda = new Agenda();
        }else{
            preencherActivity();
        }

    }

    private void preencherActivity() {
        nomeTxt.setText(agenda.getNome());
        enderecoTxt.setText(agenda.getEndereço());
        telefoneTxt.setText(agenda.getTelefone());
        celularTxt.setText(agenda.getCelular());

        if(agenda.getImagem() != null) {
            Bitmap fotoOriginal = BitmapFactory.decodeFile(agenda.getImagem());

            Bitmap fotoReduzida = Bitmap.createScaledBitmap(fotoOriginal, 200, 200, true);

            imageView.setImageBitmap(fotoReduzida);
        }

    }

    public void salvar(View view){
        String nome = nomeTxt.getText().toString();
        String endereco = enderecoTxt.getText().toString();
        String telefone = telefoneTxt.getText().toString();
        String celular = celularTxt.getText().toString();
        String imagem = nomeArquivo;


        agenda.setNome(nome);
        agenda.setImagem(imagem);
        agenda.setEndereço(endereco);
        agenda.setTelefone(telefone);
        agenda.setCelular(celular);

        try {
            dao = new AgendaDAO(this);
            dao.salvar(agenda);
            dao.close();

            Toast.makeText(NovoActivity.this, "Salvo com Sucesso", Toast.LENGTH_LONG).show();
        }catch (Exception ex){
            Toast.makeText(NovoActivity.this, "Erro ao Salvar", Toast.LENGTH_LONG).show();
        }

        finish();
    }

    public void pegarImagem(View view){
        Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);

        String caminhoDoArquivo = Environment.getExternalStorageDirectory().toString() ;


        nomeArquivo = caminhoDoArquivo + "/" + System.currentTimeMillis() + ".png";

        File arquivoImagem = new File(nomeArquivo) ;

        Uri localDeArmazenamento = Uri.fromFile( arquivoImagem ) ;

        intent.putExtra(MediaStore.EXTRA_OUTPUT , localDeArmazenamento ) ;


        startActivityForResult(intent , CAPTURA_IMAGEM);
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(CAPTURA_IMAGEM == requestCode){
            if(resultCode == RESULT_OK){


                Bitmap fotoOri = BitmapFactory.decodeFile(nomeArquivo) ;

                Bitmap fotoFormt = Bitmap.createScaledBitmap(fotoOri , 200 , 200 , true);

                imageView.setImageBitmap(fotoFormt);

            }
        }
    }
}
