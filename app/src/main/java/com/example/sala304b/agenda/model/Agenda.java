package com.example.sala304b.agenda.model;


import java.io.Serializable;

public class Agenda implements Serializable{

    private int id;
    private String nome;
    private String imagem;
    private String endereço;
    private String telefone;
    private String celular;

    public Agenda() {
    }

    public Agenda(String nome, String imagem, String endereço, String telefone, String celular) {
        this.nome = nome;
        this.imagem = imagem;
        this.endereço = endereço;
        this.telefone = telefone;
        this.celular = celular;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getImagem() {
        return imagem;
    }

    public void setImagem(String imagem) {
        this.imagem = imagem;
    }

    public String getEndereço() {
        return endereço;
    }

    public void setEndereço(String endereço) {
        this.endereço = endereço;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    public String getCelular() {
        return celular;
    }

    public void setCelular(String celular) {
        this.celular = celular;
    }

    @Override
    public String toString() {
        return this.nome;
    }
}
